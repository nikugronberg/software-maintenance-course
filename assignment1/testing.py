import unittest
import fibonacci

#normal usage tests
class TestDefaultBehaviour(unittest.TestCase):
    def testDefaultBehaviour(self):
        self.assertEqual(fibonacci.fib(0, 10), 34)
        
    def testDefaultBehaviourWithoutLength(self):
        self.assertEqual(fibonacci.fib(0), 34)

    def testDefaultBehaviourStartingFrom15(self):
        self.assertEqual(fibonacci.fib(15), 1597)

#incorrect inputs tests
class TestIncorrectInputs(unittest.TestCase):
    def testNotIntAsStart(self):
        with self.assertRaises(TypeError):
            fibonacci.fib("a")

    def testNegativeLength(self):
        self.assertEqual(fibonacci.fib(0, -1), 0)

#output.txt tests
class TestFileOutput(unittest.TestCase):
    def testOutputFileLengthEqualsLength(self):
        length = 5
        fileLineCount = 0
        fibonacci.fib(0, length)
        
        with open("output.txt", "r") as f:
            for line in f:
                fileLineCount += 1
                
        self.assertEqual(length, fileLineCount)

    def testOutputFileEqualsReturnedValue(self):
        returnValue = fibonacci.fib(0, 20)
        fileValue = -1
        with open("output.txt", "r") as f:
            for line in f:
                fileValue = int(line.split(":")[2])
        self.assertEqual(returnValue, fileValue)
        


if __name__ == "__main__":
    unittest.main()
