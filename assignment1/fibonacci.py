def fib(start, length = 10):
    first = 0
    second = 1
    offset = 0

    if start < 0 or length <= 0:
        return 0

    try:
        f = open("output.txt", "w")

        while first < start:
            temp = first
            first += second
            second = temp
            offset += 1

        for i in range(0, length):
            print("index:", i + offset, "number:", first)
            f.write("index: " + str(i + offset) + " number: " + str(first) + "\n")
            temp = first
            first += second
            second = temp
        f.close()
            
    except OSError as e:
        print(e)
    return second
        


def main():
    try:
        start = int(input("Start? "))   
    except ValueError as e:
        print(e)
        return
    try:
        length = int(input("Length? "))
    except ValueError as e:
        print(e)
        length = 10

    fib(start, length)


if __name__ == "__main__":
    main()
    
